<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model {
    public function ratings() {
        return $this->hasMany(Rating::class)->orderByDesc('id');
    }

    public function currentElo() {
        return $this->hasMany(Rating::class)->latest()->first()->elo;
    }

    public function newRating($opponents_elo, $result) {
        $own_elo = $this->currentElo();
        $expected = $this->expectedWinningPercentage($opponents_elo, $own_elo);
        return round($own_elo + (16 * ($result - $expected)));
    }

    public function expectedWinningPercentage($opponents_elo, $own_elo) {
        return 1 / (1 + pow(10, ($opponents_elo - $own_elo) / 400));
    }
}
