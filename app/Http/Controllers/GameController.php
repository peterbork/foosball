<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GameController extends Controller {
    public function generateTeams($players) {
        $players = $players->sortBy('elo')->values();

        $setSize = $players->count() / 2;
        $pos1 = 0;
        $pos2 = 0;
        $i = $players->count() - 1;
        $sum1 = 0;
        $sum2 = 0;

        while ($pos1 < $setSize && $pos2 < $setSize) {
            if ($sum1 < $sum2) {
                $set1[$pos1] = $players->get($i);
                $pos1++;
                $sum1 += $players->get($i)->ratings->first()->elo;
            } else {
                $set2[$pos2] = $players->get($i);
                $pos2++;
                $sum2 += $players->get($i)->ratings->first()->elo;
            }
            $i--;
        }

        while ($i >= 0) {
            if ($pos1 < $setSize) {
                $set1[$pos1++] = $players->get($i);
            } else {
                $set2[$pos2++] = $players->get($i);
            }
            $i--;
        }
        return [
            'a' => $set1,
            'b' => $set2
        ];
    }

    public function simulate($teams, $result) {
        $first_team = $teams['a'];
        $second_team = $teams['b'];

        if ($result == 'a') {

        }
    }
}
